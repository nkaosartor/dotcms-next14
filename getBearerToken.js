const { initDotCMS } = require('dotcms');

export const dotCMS = initDotCMS({
	host: process.env.NEXT_PUBLIC_DOTCMS_HOST,
	token: process.env.BEARER_TOKEN,
});

const getAuthToken = async ({ user, password, expirationDays, host }) => {
	const token = await dotCMS.auth
		.getToken({ user, password, expirationDays, host })
		.then((res) => res)
		.catch((err) => {
			if (err.status === 400 || err.status === 401) {
				console.info('\n');
				printError(err.message);
				return;
			}
			throw err;
		});

	console.log('[DOTCMS TOKEN]', token);

	return token;
};

// Command-line arguments
const user = process.argv[2];
const password = process.argv[3];
const expirationDays = 100;
const host = 'https://instride.dotcms.io';

if (!user || !password) {
	console.error(
		'Usage: node set-bearer-token.js <user> <password> [expirationDays]'
	);
	process.exit(1);
}

try {
	console.log('Getting token...');
	console.log({ user, password, expirationDays, host });
	getAuthToken({ user, password, expirationDays, host });
} catch (e) {
	console.log(e);
}
